export default {
  en: {
    home: {
      welcome: 'Partagez vos traces de VTT ou de Trail, évaluez votre parcours,<br>ajoutez des photos ou des vidéos puis partagez les sur les réseaux sociaux!'
    },
    menu: {
      search: 'Search',
      blog: 'Blog',
      upload: 'Upload',
      login: 'Login',
      register: 'Register',
      logout: 'Logout'
    },
    footer: {
      home: 'Home',
      login: 'Login',
      twitter: 'Twitter',
      email: 'Email',
      facebook: 'Facebook',
      gpx: 'GPX File format',
      gpsBabel: 'GPSBabel an essential tool',
      sendMessage: 'Send message',
      yourMessage: 'Your message'
    },
    trackView: {
      distance: 'distance',
      elevation: 'elevation',
      downloads: 'downloads',
      edit: 'Edit',
      delete: 'Delete',
      search: 'Search Around',
      download: 'Download',
      start: 'Start',
      end: 'End',
      trackUploaded: 'Track uploaded by',
      on: 'on'
    },
    trackEdit: {
      description: 'distance',
      elevation: 'elevation'
    },
    search: {
      distanceMin: 'Min Distance',
      distanceMax: 'Max Distance',
      elevationMax: 'Max Elevation',
      details: 'Details',
      cancel: 'Cancel',
      filter: 'Filter',
      city: 'Enter name of a city'
    },
    login: {
      login: 'Login',
      email: 'Email',
      passowrd: 'Password'
    },
    register: {
      success: 'Thank you ! An email has been sent you can now <router-link :to="{name: "login"}">log in.</router-link>',
      email: 'Email',
      accountCreated: 'Account created',
      login: 'Login',
      password: 'Password',
      name: 'Name',
      emailNotValid: 'Email is not valid',
      passwordSize: 'Password should be at least 8 characters',
      passwordAgain: 'Type password again',
      passwordDoesntMatch: 'Password doesn\'t match',
      signup: 'Sign Up',
      accountAlreadyExists: 'Username or email already exists'
    }
  }
}
