# la-trace-v3

[![pipeline status](https://gitlab.com/la-trace/front-v3/badges/master/pipeline.svg)](https://gitlab.com/la-trace/front-v3/commits/master)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
