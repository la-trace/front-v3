var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // API: '"http://localhost:8000/"',
  API: '"https://la-trace.com/api/"',
  RESIZE: '"https://v3.la-trace.com/img/"',
  ELEVATION: '"https://elevations.la-trace.com/srtmgl1/elevations"'
})
