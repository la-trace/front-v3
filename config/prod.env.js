module.exports = {
  NODE_ENV: '"production"',
  COMMIT: JSON.stringify(process.env.COMMIT),
  API: '"https://la-trace.com/api/"',
  RESIZE: '"https://la-trace.com/img/"',
  ELEVATION: '"https://elevations.la-trace.com/srtmgl1/elevations"'
}
